<?php

namespace Cimp;

class Category
{
    /** @var string */
    public $name;

    /** @var int */
    public $level = 0;

    /** @var int */
    public $id;

    /**
     * Category constructor.
     * @param string $cellValue
     * @param int $level
     * @param bool $trimName
     * @throws \Exception
     */
    public function __construct( $cellValue, $level, $trimName = true )
    {
        if ( $trimName ) {
            $data = [];
            preg_match( '/[А|A]\s*([А-Я0-9A-Z]{1,4}\.\s*)*/u', $cellValue, $data );

            $this->name = preg_replace( '/' . $data[0] . '/', '', $cellValue, 1 );

            preg_match( '/^[А-Я0-9A-Z]{1,2}\s+/u', $this->name, $data );

            if ( $data[0] ) {
                $this->name = preg_replace( '/' . $data[0] . '/', '', $this->name, 1 );
            }

            $this->name = trim( $this->name );
        } else {
            $this->name = $cellValue;
        }

        $this->level = $level;

        if ( $this->level < 0 ) {
            throw new \Exception( "Found category with level < 1: $cellValue" );
        }
    }
}