<?php
/** @var \Cimp\ImportProgress $progress */
?>

<h1>Загрузка каталога</h1>

<form method="post" enctype="multipart/form-data">
    <?php if ( isset( $error ) ): ?>
        <div class="notice notice-error">
            <p><?= $error ?></p>
        </div>
    <?php endif; ?>

    <div id="error_wrap" style="<?= !$progress->lastError ? 'display: none;' : '' ?>margin-top: 30px;">
        There was an error during script work. Please contact ilya.chase@yanex.ru with following:
        <p><code><?= esc_html( $progress->lastError ) ?></code></p>
    </div>

    <div id="progress" style="margin-top: 30px;<?= !$progress->total ? ' display: none;' : '' ?>">
        <p>Всего строк: <strong id="total_results"><?= $progress->total ?></strong></p>
        <p>Обработано строк: <strong id="current"><?= $progress->current ?></strong></p>
    </div>

    <input type="file" name="<?= \Cimp\CimpPlugin::FILE_FIELD_NAME ?>"/>
    <?php submit_button( 'Загрузить' ) ?>
</form>

<script type="text/javascript">
    var task_finished = <?= json_encode( $progress->finished || $progress->lastError ) ?>;

    var $ = jQuery;

    $( function () {
        if ( task_finished )
            return;

        delayedRecursiveUpdater( $( '#progress_bar' ) );
    } );

    function delayedRecursiveUpdater( $progressBar ) {
        setTimeout( function () {
            $.ajax( '/wp-content/plugins/catalog_import/status.php', {
                success: function ( data ) {
                    if ( data.lastError ) {
                        $( '#error_wrap code' ).text( data.lastError );
                        $( '#error_wrap' ).show();
                        window.task_finished = true;
                        return;
                    }

                    if ( data.lastAction > 0 ) {
                        $( '#progress' ).show();
                    }

                    $( '#total_results' ).text( data.total );
                    $( '#current' ).text( data.current );

                    if ( parseInt( data.progress ) === 100 ) {
                        return;
                    }

                    delayedRecursiveUpdater( $progressBar );
                }
            } );
        }, 1000 );
    }
</script>