<?php

namespace Cimp;

use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

class Importer
{
    const HEADER_COLUMNS_COUNT = 43;
    const CATEGORY_MARKER = 'А';
    const CATEGORY_MARKER_2 = 'A';
    const BOTTOM_GARBAGE_ROWS_COUNT = 5;

    /**
     * @param array $files
     * @return null|string
     */
    public function uploadAndStartTask( array $files )
    {
        if ( !$files ) {
            return null;
        }

        if (
            !isset( $files[CimpPlugin::FILE_FIELD_NAME] )
            || !isset( $files[CimpPlugin::FILE_FIELD_NAME]['error'] )
            || !isset( $files[CimpPlugin::FILE_FIELD_NAME]['tmp_name'] )
            || !isset( $files[CimpPlugin::FILE_FIELD_NAME]['name'] )
            || $files[CimpPlugin::FILE_FIELD_NAME]['error'] !== 0 ) {
            return 'Произошла ошибка при загрузке файла.';
        }

        $ext = '';
        preg_match( '/.*\.(.*)/u', $files[CimpPlugin::FILE_FIELD_NAME]['name'], $m );
        if ( isset( $m[1] ) ) {
            $ext = trim( $m[1] );
        }

        $pricePath = CIMP_DIR . DS . 'uploads' . DS . 'price.' . $ext;
        move_uploaded_file( $files[CimpPlugin::FILE_FIELD_NAME]['tmp_name'], $pricePath );

        $command = ( new PhpExecutableFinder() )->find() . ' ' . CIMP_DIR . DS . 'task.php ' . $pricePath . ' 2> ' . CIMP_DIR . DS . 'uploads' . DS . 'log.txt &';
        $process = new Process( $command );
        $process->disableOutput();
        $process->run();

        return null;
    }

    /**
     * @param string $pricePath
     * @param int $offset
     * @throws \Exception
     */
    public function task( $pricePath, $offset = 0 )
    {
        $progress = new ImportProgress();
        $progress->save( true );
        ini_set( 'memory_limit', '2G' );

        try {
            $this->_clearWooCommerce();

            $filetype = \PhpOffice\PhpSpreadsheet\IOFactory::identify( $pricePath );
            $readerClass = "\PhpOffice\PhpSpreadsheet\Reader\\$filetype";
            /** @var \PhpOffice\PhpSpreadsheet\Reader\Xls|\PhpOffice\PhpSpreadsheet\Reader\Xlsx $reader */
            $reader = new $readerClass();
            $spreadsheet = $reader->load( $pricePath );
            $progress->total = $spreadsheet->getActiveSheet()->getHighestDataRow() - self::HEADER_COLUMNS_COUNT - self::BOTTOM_GARBAGE_ROWS_COUNT;
            $progress->save( true );

            $categoriesStack = new \SplStack();
            foreach ( $spreadsheet->getActiveSheet()->getColumnIterator()->seek( 'B' )->current()->getCellIterator() as $k => $cell ) {
                if ( $k <= self::HEADER_COLUMNS_COUNT ) {
                    $progress->current++;
                    continue;
                }

                if ( $offset && $k <= $offset ) {
                    continue;
                }

                $cellValue = trim( $cell->getValue() );
                if ( mb_strpos( $cellValue, "ОСТАТКИ РАСПРОДАЖА" ) !== false ) {
                    $progress->current = $progress->total;
                    $progress->save( true );
                    break;
                }

                $cellValue = trim( $cellValue, '+' );
                if ( strncmp( $cellValue, self::CATEGORY_MARKER, strlen( self::CATEGORY_MARKER ) ) === 0 || strncmp( $cellValue, self::CATEGORY_MARKER_2, strlen( self::CATEGORY_MARKER_2 ) ) === 0 ) {
                    $currentCategory = new Category( $cellValue, $spreadsheet->getActiveSheet()->getRowDimension( $k )->getOutlineLevel() );
                    if ( !$currentCategory->name ) {
                        $currentCategory = new Category( $cellValue, $spreadsheet->getActiveSheet()->getRowDimension( $k )->getOutlineLevel(), false );
                    }

                    if ( $currentCategory->level == 0 ) {
                        $currentCategory->id = $this->_addCategory( $currentCategory->name, 0 );
                        $categoriesStack = new \SplStack();
                        $categoriesStack->push( $currentCategory );
                        $progress->current++;
                        $progress->save();
                        continue;
                    }

                    if ( $categoriesStack->isEmpty() ) {
                        throw new \Exception( "Найдена некорневая категория, но перед ней не было корневой: " . $currentCategory->name );
                    }

                    /** @var Category $prevCategory */
                    $prevCategory = $categoriesStack->top();

                    if ( $currentCategory->level > $prevCategory->level ) {
                        $currentCategory->id = $this->_addCategory( $currentCategory->name, $prevCategory->id );
                        $categoriesStack->push( $currentCategory );
                    } elseif ( $currentCategory->level == $prevCategory->level ) {
                        $categoriesStack->pop();
                        $prevCategory = $categoriesStack->top();
                        $currentCategory->id = $this->_addCategory( $currentCategory->name, $prevCategory->id );
                        $categoriesStack->push( $currentCategory );
                    } else {
                        for ( $i = 0; $i <= $prevCategory->level - $currentCategory->level; $i++ ) {
                            $categoriesStack->pop();
                        }
                        $prevCategory = $categoriesStack->top();
                        $currentCategory->id = $this->_addCategory( $currentCategory->name, $prevCategory->id );
                        $categoriesStack->push( $currentCategory );
                    }
                } else {
                    if ( $categoriesStack->isEmpty() ) {
                        throw new \Exception( "В прайсе найден товар без родительской категории на строке $k: " . $cellValue );
                    }

                    $quantity = $spreadsheet->getActiveSheet()->getCell( "C$k" )->getValue();
                    $price = $spreadsheet->getActiveSheet()->getCell( "D$k" )->getValue();

                    /** @var Category $prevCategory */
                    $prevCategory = $categoriesStack->top();
                    $this->_addProduct( $cellValue, $prevCategory->id, $price, $quantity );
                }

//                er( "$k / $progress->total" );
//                er( memory_get_usage( true ) / 1024 / 1024 );

                $progress->current++;
                $progress->save();
                if ( $k % 500 == 0 ) {
                    wp_cache_flush();
                }
            }
        } catch ( \Exception $e ) {
            $progress->lastError = $e->getMessage();
            $progress->finished = true;
            $progress->save( true );
            throw $e;
        }

        $progress->finished = true;
        $progress->save( true );
    }

    /**
     * @param string $name
     * @param int $parent
     * @param string $description
     * @return int
     * @throws \Exception
     */
    private function _addCategory( $name, $parent = 0, $description = '' )
    {
        $result = wp_insert_term( $name, 'product_cat', [
                'description' => $description,
                'slug' => sanitize_title( $this->_rus2translit( $name ) ),
                'parent' => $parent
            ]
        );

        if ( $result instanceof \WP_Error ) {

            if ( isset( $result->errors['term_exists'] ) ) {
                for ( $i = 0; $i < 100; $i++ ) {
                    $result = wp_insert_term( $name . " ($i)", 'product_cat', [
                            'description' => $description,
                            'slug' => sanitize_title( $this->_rus2translit( $name ) . " ($i)" ),
                            'parent' => $parent
                        ]
                    );

                    if ( !$result instanceof \WP_Error ) {
                        return (int)$result['term_id'];
                    }
                }
            }

            throw new \Exception( "Что-то пошло не так при создании категории: " . $result->get_error_message() );
        }

        return (int)$result['term_id'];
    }

    /**
     * @param string $name
     * @param int $parent
     * @param float $price
     * @param int $quantity
     * @param string $description
     */
    private function _addProduct( $name, $parent = 0, $price, $quantity, $description = '' )
    {
        $post_id = wp_insert_post( array(
            'post_author' => 1,
            'post_title' => $name,
            'post_content' => $description,
            'post_status' => 'publish',
            'post_type' => "product",
            'post_name' => sanitize_title( $this->_rus2translit( $name ) ),
        ) );
        wp_set_object_terms( $post_id, 'simple', 'product_type' );
        update_post_meta( $post_id, '_price', $price );
        update_post_meta( $post_id, '_regular_price', $price );
        update_post_meta( $post_id, '_stock_status', 'instock' );
        update_post_meta( $post_id, '_downloadable', 'no' );
        update_post_meta( $post_id, '_manage_stock', 'yes' );
        update_post_meta( $post_id, '_stock', $quantity );
        wp_set_object_terms( $post_id, $parent, 'product_cat' );
    }

    private function _clearWooCommerce()
    {
        global $wpdb;

        $pref = $wpdb->prefix;

        $wpdb->query( "DELETE a,c FROM {$pref}terms AS a 
              LEFT JOIN {$pref}term_taxonomy AS c ON a.term_id = c.term_id
              LEFT JOIN {$pref}term_relationships AS b ON b.term_taxonomy_id = c.term_taxonomy_id
              WHERE c.taxonomy = 'product_tag'" );
        $wpdb->query( "DELETE a,c FROM {$pref}terms AS a
              LEFT JOIN {$pref}term_taxonomy AS c ON a.term_id = c.term_id
              LEFT JOIN {$pref}term_relationships AS b ON b.term_taxonomy_id = c.term_taxonomy_id
              WHERE c.taxonomy = 'product_cat'" );

        $wpdb->query(
            "DELETE relations.*, taxes.* , terms.* 
            FROM " . $pref . "term_relationships AS relations
        INNER JOIN " . $pref . "term_taxonomy AS taxes
        ON relations.term_taxonomy_id=taxes.term_taxonomy_id
        INNER JOIN " . $pref . "terms AS terms
        ON taxes.term_id=terms.term_id
        WHERE object_id IN (SELECT ID FROM " . $pref . "posts WHERE post_type='product');"
        );

        $wpdb->query(
            "DELETE FROM " . $pref . "postmeta WHERE post_id IN (SELECT ID FROM " . $pref . "posts WHERE post_type = 'product');"
        );

        $wpdb->query(
            "DELETE FROM " . $pref . "postmeta WHERE post_id IN (SELECT ID FROM " . $pref . "posts WHERE post_type = 'product_variation');"
        );

        $wpdb->query(
            "DELETE FROM " . $pref . "posts WHERE post_type = 'product';"
        );

        $wpdb->query(
            "DELETE FROM " . $pref . "posts WHERE post_type = 'product_variation';"
        );
    }

    /**
     * @param string $string
     * @return string
     */
    private function _rus2translit( $string )
    {
        $string = mb_strtolower( $string );

        $converter = array(
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ь' => '',
            'ы' => 'y',
            'ъ' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
        );
        return strtr( $string, $converter );
    }

    /**
     * @return bool
     */
    public static function IsWindows()
    {
        return strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN';
    }

    /**
     * @return bool
     */
    public static function IsCli()
    {
        return php_sapi_name() == "cli";
    }
}