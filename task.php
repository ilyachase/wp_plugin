<?php

if ( !isset( $argv[1] ) ) {
    exit( 1 );
}

require_once 'require.php';
( new \Cimp\Importer() )->task( $argv[1], isset( $argv[2] ) ? (int)$argv[2] : 0 );
