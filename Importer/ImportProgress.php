<?php

namespace Cimp;

class ImportProgress
{
    const FILENAME = 'progress.txt';

    const BUFFERED_SAVE_COUNTER = 10;

    /** @var bool */
    public $finished = false;

    /** @var int */
    public $lastAction = 0;

    /** @var int */
    public $total = 0;

    /** @var int */
    public $current = 0;

    /** @var string */
    public $lastError;

    /** @var int */
    public $progress = 0;

    /** @var bool */
    protected $bufferedSave = true;

    /** @var int */
    protected $internalSaveCounter = 0;

    public function __construct( $bufferedSave = true )
    {
        if ( $this->total ) {
            $this->progress = (int)( $this->current / $this->total * 100 );
        };

        $this->bufferedSave = $bufferedSave;
    }

    public function __wakeup()
    {
        if ( $this->total ) {
            $this->progress = (int)( $this->current / $this->total * 100 );
        };
    }

    /**
     * @param bool $forceSave
     */
    public function save( $forceSave = false )
    {
        $this->lastAction = time();

        if ( $this->bufferedSave && !$forceSave ) {
            $this->internalSaveCounter++;

            if ( $this->internalSaveCounter < self::BUFFERED_SAVE_COUNTER && $this->current < $this->total ) {
                return;
            } else {
                $this->internalSaveCounter = 0;
            }
        }

        file_put_contents( CIMP_DIR . DS . 'uploads' . DS . self::FILENAME, serialize( $this ) );
    }

    /**
     * @return static
     */
    public static function Load()
    {
        $filename = CIMP_DIR . DS . 'uploads' . DS . self::FILENAME;
        if ( !file_exists( $filename ) ) {
            return new static();
        }

        return unserialize( file_get_contents( $filename ) );
    }

    public static function Purge()
    {
        @unlink( CIMP_DIR . DS . 'uploads' . DS . self::FILENAME );
    }
}