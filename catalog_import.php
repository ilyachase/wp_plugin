<?php
/*
Plugin Name: Catalog Import
*/

require_once 'require.php';

function dispath_request()
{
    ( new Cimp\CimpPlugin() )->dispatch();
}

function cimp_admin_page()
{
    add_menu_page(
        'Импорт каталога',
        'Импорт каталога',
        'manage_options',
        'cimp',
        'dispath_request'
    );
}

add_action( 'admin_menu', 'cimp_admin_page' );