<?php

namespace Cimp;

class CimpPlugin
{
    const FILE_FIELD_NAME = 'catalog_file';

    public function dispatch()
    {
        $error = ( new Importer() )->uploadAndStartTask( $_FILES );
        $progress = ImportProgress::Load();

        require 'view.php';
    }
}