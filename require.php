<?php

const CIMP_DIR = __DIR__;
const DS = DIRECTORY_SEPARATOR;

require_once 'CimpPlugin.php';
require_once 'Importer/Importer.php';
require_once 'Importer/ImportProgress.php';
require_once 'Importer/Category.php';
require_once 'vendor/autoload.php';

if ( \Cimp\Importer::IsCli() ) {
    require_once CIMP_DIR . DS . '../../../wp-load.php';
}
